import React, { Component } from 'react';
import Keycloak from 'keycloak-js';

class Secured extends Component {

    constructor(props) {
        super(props);
        this.state = { keycloak: null, authenticated: false, token: null, weatherForecasts: [], loadingData: false };
    }

    componentDidMount() {
        // Create an instance of the keyloak object using the keycloak.json settings. This is what points to your running instance.
        // init will check if the user is logged in and if not redirect to the login page of the instance  
        const keycloak = Keycloak('/keycloak.json');
        keycloak.init({ onLoad: 'login-required' }).then(authenticated => {
            // stores the keycloak object and token
            this.setState({ keycloak: keycloak, authenticated: authenticated, token: keycloak.token })
            // Attempts to get data from the API
            this.getWeatherForecast()
        })
    }

    getWeatherForecast() {
        // Attached the token to the request
        const URL = "insert API url here"
        fetch(URL, {
            method: 'GET',
            headers: {
                authorization: `Bearer ${this.state.token}`
            }
        }).then(response => response.json())
            .then(response => this.setState({ weatherForecasts: response }))
    }


    render() {
        if (this.state.keycloak) {
            if (this.state.authenticated) return (
                <div> <div>
                    <p>This is a Keycloak-secured component of your application. You shouldn't be able
          to see this unless you've authenticated with Keycloak.</p>
                </div>
                    <div>
                        {this.state.token && <p>Token Received 🐱‍💻</p>}
                    </div>
                    <div>
                        {this.state.weatherForecasts.map((forecast, index) => { return <li key={index}>{forecast.summary}</li> })}
                    </div>
                </div>

            ); else return (<div>Unable to authenticate!</div>)
        }
        return (
            <div>Initializing Keycloak...</div>
        );
    }
}
export default Secured;